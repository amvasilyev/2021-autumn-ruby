animals = %w(ant bee cat dog)
pp animals
# animals.each { |animal| puts animal }
animals.each do |animal|
    puts animal
end
animal_count = 0
animals.each {|_animal| animal_count += 1}
puts "Animal count: #{animal_count}"

3.upto(6) { |number| print number }
puts

('a'..'e').each { |character| print character }
puts

nums = 100.upto(500).select { |number| number%17 == 0 }.sum
pp nums