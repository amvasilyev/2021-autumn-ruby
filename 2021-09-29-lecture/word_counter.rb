# frozen_string_literal: true

require 'byebug'

TEXT = <<~DOC
  What is Lorem Ipsum?

  Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
DOC

def text_to_words(text)
  text.downcase.scan(/[\w']+/)
end

def count_words(word_list)
  frequency = Hash.new(0)
  word_list.each do |word|
    frequency[word] += 1
  end
  frequency
end

def main
  words = text_to_words(TEXT)
  frequencies = count_words(words)
  sorted = frequencies.sort_by { |_word, count| count }
  puts sorted
end

main if $PROGRAM_NAME == __FILE__
