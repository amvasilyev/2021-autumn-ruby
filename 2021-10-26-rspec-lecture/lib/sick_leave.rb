# frozen_string_literal: true

# The sick leave for a person given in a hospital
class SickLeave
  attr_reader :doctor

  # rubocop:disable Metrics/ParameterLists
  def initialize(id, patient_name, patient_sex, period_start, period_end,
                 leave_reason, doctor)
    @id = id
    @patient_name = patient_name
    @patient_sex = patient_sex
    @period_start = period_start
    @period_end = period_end
    @leave_reason = leave_reason
    @doctor = doctor
  end
  # rubocop:enable Metrics/ParameterLists

  DAY_FORMAT = '%Y-%m-%d'

  def to_s
    "#{@patient_name} #{posibility_code} возможности посещать своё рабочее "\
      "место в период с #{@period_start.strftime(DAY_FORMAT)} по "\
      "#{@period_end.strftime(DAY_FORMAT)} по причине #{leave_reason}, "\
      "код листа нетрудоспособности #{@id}"
  end

  def male?
    @patient_sex == :male
  end

  def sex?(sex)
    @patient_sex == sex
  end

  DAY_DURATION = 60 * 60 * 24

  def duration
    ((@period_end - @period_start) / DAY_DURATION) + 1.0
  end

  def ends_in?(year)
    @period_end.year == year
  end

  private

  LEAVE_REASONS = [
    'Заболевание',
    'Травма',
    'Карантин',
    'Несчастный случай на производстве или его последствия',
    'Отпуск по беременности и родам',
    'Протезирование в стационаре',
    'Профессиональное заболевание или его обострение',
    'Долечивание в санатории',
    'Уход за больным членом семьи',
    'Иное состояние (отравление, проведение манипуляций и др.)'
  ].freeze

  def leave_reason
    LEAVE_REASONS[@leave_reason - 1]
  end

  def posibility_code
    if @patient_sex == :female
      'не имела'
    else
      'не имел'
    end
  end
end
