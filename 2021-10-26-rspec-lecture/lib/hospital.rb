# frozen_string_literal: true

# The operations in the hospital
class Hospital
  def initialize(sick_leaves = [])
    @sick_leaves = sick_leaves
  end

  def random_sick_leave
    @sick_leaves.sample
  end

  def doctors
    all_doctors = @sick_leaves.map(&:doctor)
    all_doctors.sort
  end

  def sex_statistics
    female_sick_leaves = @sick_leaves.select do |sick_leave|
      sick_leave.sex?(:female)
    end
    {
      all: @sick_leaves.size,
      female: female_sick_leaves.size,
      male: @sick_leaves.size - female_sick_leaves.size
    }
  end

  def average_sick_leave_duration(year = nil)
    sick_leaves = if year
                    @sick_leaves.select { |leave| leave.ends_in?(year) }
                  else
                    @sick_leaves
                  end
    return 0 if sick_leaves.empty?

    sick_leave_durations = sick_leaves.map(&:duration)
    sick_leave_durations.sum / sick_leave_durations.size
  end
end
