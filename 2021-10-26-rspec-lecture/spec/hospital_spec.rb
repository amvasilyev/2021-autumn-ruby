# frozen_string_literal: true

require 'hospital'
require 'sick_leave'

RSpec.describe Hospital do
  let(:sick_leave_one) do
    SickLeave.new(
      '535 755 753', 'Ivanov Petr', :male,
      Time.new(2020, 10, 15), Time.new(2020, 10, 22), 5,
      'Jekyll'
    )
  end

  let(:sick_leave_two) do
    SickLeave.new(
      '657 537 575', 'Petrov Igor', :male,
      Time.new(2021, 10, 1), Time.new(2021, 10, 15), 2,
      'House'
    )
  end

  let(:sick_leave_three) do
    SickLeave.new(
      '537 537 227', 'Romanova Liza', :female,
      Time.new(2021, 10, 2), Time.new(2021, 10, 20), 1, 'Popov'
    )
  end

  let(:hospital) do
    Hospital.new([sick_leave_one, sick_leave_two, sick_leave_three])
  end

  it 'should return a random patient' do
    expect(hospital.random_sick_leave).to be_a(SickLeave)
  end

  it 'should provide a list of doctors' do
    expect(hospital.doctors).to eq([
                                     sick_leave_two.doctor,
                                     sick_leave_one.doctor,
                                     sick_leave_three.doctor
                                   ])
  end

  it 'should provide statistics about sick leaves according to the sex' do
    expect(hospital.sex_statistics).to eq({ all: 3, male: 2, female: 1 })
  end

  it 'should provide average length for all sick_leaves' do
    expect(hospital.average_sick_leave_duration).to eq(14)
  end

  it 'should provide average length for sick leaves ended in 2021' do
    expect(hospital.average_sick_leave_duration(2021)).to eq(17)
  end

  it 'should provide average length for unknown year' do
    expect(hospital.average_sick_leave_duration(1000)).to eq(0)
  end
end
