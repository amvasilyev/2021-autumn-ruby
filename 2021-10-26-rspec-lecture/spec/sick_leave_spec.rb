# frozen_string_literal: true

require 'sick_leave'

RSpec.describe SickLeave do
  let(:sick_leave) do
    SickLeave.new(
      '573 575 537', 'Ivanov Ivan', :male,
      Time.new(2021, 10, 1), Time.new(2021, 10, 21), 3,
      'Popov'
    )
  end

  let(:sick_leave_female) do
    SickLeave.new(
      '666 555 222', 'Diana Petrova', :female,
      Time.new(2021, 9, 20), Time.new(2021, 9, 27), 2, 'Jekyll'
    )
  end

  it 'should provide a name of a doctor' do
    expect(sick_leave.doctor).to eq('Popov')
  end

  it 'should provide information in text format for male' do
    expect(sick_leave.to_s).to(
      eq(
        'Ivanov Ivan не имел возможности посещать '\
        'своё рабочее место в период с 2021-10-01 по 2021-10-21 по причине '\
        'Карантин, код листа нетрудоспособности 573 575 537'
      )
    )
  end

  it 'should provide information in text format for female' do
    expect(sick_leave_female.to_s).to(
      eq(
        'Diana Petrova не имела возможности посещать '\
        'своё рабочее место в период с 2021-09-20 по 2021-09-27 по причине '\
        'Травма, код листа нетрудоспособности 666 555 222'
      )
    )
  end

  it 'should tell whether sick leave is given to male or female' do
    expect(sick_leave.male?).to be_truthy
  end

  [
    [:sick_leave, :male, true],
    [:sick_leave, :female, false],
    [:sick_leave_female, :male, false],
    [:sick_leave_female, :female, true]
  ].each do |sick_leave_code, sex, result|
    it 'should tell whether sick leave is given to specified sex' do
      test_sick_leave = method(sick_leave_code).call
      expect(test_sick_leave.sex?(sex)).to be(result)
    end
  end

  it 'should provide a duration for a leave' do
    expect(sick_leave.duration).to eq(21)
  end

  it 'should check whether leave ends in the specified year' do
    expect(sick_leave.ends_in?(2021)).to be(true)
  end

  it 'should check whether leave ends in the specified year' do
    expect(sick_leave.ends_in?(2010)).to be(false)
  end
end
