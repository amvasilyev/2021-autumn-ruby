# frozen_string_literal: true

require 'sick_leave'

RSpec.describe SickLeave do
  let(:sick_leave) do
    SickLeave.new('553 75', 'Petrov', :male, Time.new(2010, 9, 15),
                  Time.new(2010, 9, 25), 15, 'Bashirov')
  end

  it 'should provide a doctor name' do
    expect(sick_leave.doctor).to eq('Bashirov')
  end

  it 'should check whether sick list is given to female' do
    expect(sick_leave.female?).to be false
  end

  it 'should provide sick leave duration in days' do
    expect(sick_leave.duration).to eq(11)
  end

  it 'should provide information about a start year' do
    expect(sick_leave.start_year).to eq(2010)
  end

  it 'should state it id' do
    expect(sick_leave.id).to eq('553 75')
  end
end
