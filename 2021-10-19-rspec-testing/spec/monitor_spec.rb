# frozen_string_literal: true

require 'monitor'

RSpec.describe Monitor do
  let(:monitor) { Monitor.new(400, 300) }

  it 'knows it diagonale' do
    expect(monitor.diagonale).to eq(500)
  end
end
