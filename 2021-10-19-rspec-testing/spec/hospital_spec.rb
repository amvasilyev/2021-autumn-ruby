# frozen_string_literal: true

require 'hospital'
require 'sick_leave'

RSpec.describe Hospital do
  let(:first_person) do
    SickLeave.new('540 445', 'Person', :male, Time.new(2011, 10, 15),
                  Time.new(2011, 10, 30), 10, 'House')
  end

  let(:second_person) do
    SickLeave.new('553 575', 'Human', :female, Time.new(2010, 10, 20),
                  Time.new(2010, 11, 4), 5, 'Popov')
  end

  let(:third_person) do
    SickLeave.new('575 577', 'Droid', :male, Time.new(2010, 10, 5),
                  Time.new(2010, 10, 14), 5, 'Popov')
  end

  let(:hospital) do
    Hospital.new([first_person, second_person, third_person])
  end

  it 'should return a random patient' do
    expect(hospital.random_sick_leave).to be_an_instance_of SickLeave
  end

  it 'should return a list of doctors' do
    doctors = [first_person.doctor, second_person.doctor,
               third_person.doctor].uniq
    expect(hospital.doctors).to match_array(doctors)
  end

  it 'should provide statistics based on sex' do
    statistics = {
      all: 3,
      male: 2,
      female: 1
    }
    expect(hospital.sick_leave_statistics).to include(statistics)
  end

  it 'should provide average duration of all sick leaves' do
    expect(hospital.average_sick_leave_duration).to eq(14)
  end

  it 'should provide average duration of sick leaves per year' do
    expect(hospital.average_sick_leave_duration(2010)).to eq(13)
  end

  it 'should provide statistics even if there is no data present' do
    expect(hospital.average_sick_leave_duration(1000)).to eq(0)
  end

  it 'should find the minimal sick leave' do
    expect(hospital.minimal_sick_leave).to eq(third_person)
  end

  it 'should find the maximal sick leave' do
    expect(hospital.maximum_sick_leave).to eq(first_person)
  end
end
