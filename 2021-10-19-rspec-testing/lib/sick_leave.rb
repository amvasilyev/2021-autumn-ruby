# frozen_string_literal: true

# Information about a sick leave of a person
class SickLeave
  attr_reader :doctor
  attr_reader :id

  # Only during development
  # rubocop:disable Metrics/ParameterLists
  def initialize(id, patient_name, sex, start_date, end_date, leave_reason_code,
                 doctor)
    @id = id.dup.freeze
    @patient_name = patient_name
    @sex = sex
    @start_date = start_date
    @end_date = end_date
    @leave_reason_code = leave_reason_code
    @doctor = doctor.dup.freeze
  end
  # rubocop:enable Metrics/ParameterLists

  def female?
    @sex == :female
  end

  SECONDS_IN_DAY = 60 * 60 * 24

  def duration
    ((@end_date - @start_date) / SECONDS_IN_DAY).round(half: :down) + 1
  end

  def start_year
    @start_date.year
  end

  TIME_FORMAT = '%Y-%m-%d'

  def to_s
    "#{@patient_name}: #{@start_date.strftime(TIME_FORMAT)}-\
#{@end_date.strftime(TIME_FORMAT)} #{duration} days. Id: #{@id}"
  end
end
