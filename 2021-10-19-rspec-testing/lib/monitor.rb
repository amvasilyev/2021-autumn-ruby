# frozen_string_literal: true

# Information about a monitor (a rectangle currently)
class Monitor
  def initialize(width, height)
    @width = width
    @height = height
  end

  def diagonale
    Math.sqrt((@width**2) + (@height**2))
  end
end
