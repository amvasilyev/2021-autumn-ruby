# frozen_string_literal: true

# The holder for the sick leaves for an entire hospital
class Hospital
  def initialize(sick_leaves = [])
    @sick_leaves = sick_leaves
  end

  def random_sick_leave
    @sick_leaves.sample
  end

  def doctors
    @sick_leaves.map(&:doctor).uniq
  end

  def sick_leave_statistics
    female_leaves = @sick_leaves.select(&:female?)
    {
      all: @sick_leaves.size,
      male: @sick_leaves.size - female_leaves.size,
      female: female_leaves.size
    }
  end

  def average_sick_leave_duration(year = nil)
    durations = if year.nil?
                  @sick_leaves
                else
                  @sick_leaves.select do |sick_leave|
                    sick_leave.start_year == year
                  end
                end
    return 0 if durations.empty?

    durations = durations.map(&:duration)
    durations.sum.to_f / durations.size
  end

  def minimal_sick_leave
    sorted_durations = @sick_leaves.sort_by(&:duration)
    target_duration = sorted_durations.first.duration
    find_by_min_id(target_duration)
  end

  def maximum_sick_leave
    sorted_durations = @sick_leaves.sort_by(&:duration)
    target_duration = sorted_durations.last.duration
    find_by_min_id(target_duration)
  end

  private

  def find_by_min_id(duration)
    desired_durations = @sick_leaves.select do |sick_leave|
      sick_leave.duration == duration
    end
    desired_durations.sort_by(&:id).first
  end
end
