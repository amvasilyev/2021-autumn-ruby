def say_goodnight(name)
    "Good night, #{name}"
end

# Вызываем метод
john_goodnight = say_goodnight("John-Boy")
puts john_goodnight
puts say_goodnight("Mary-Ellen")

# Do not do this!
# puts say_goodnight 'Home'
