def say_hello(name)
    "Hello, #{name}"
end

hello_mark = say_hello('Mark')
puts hello_mark
puts say_hello('Vasily')
