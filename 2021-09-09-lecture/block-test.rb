def call_block
    puts 'До вызова блока'
    yield
    puts 'После вызова блока'
end

call_block { puts 'Внутри блока' }

def who_says_what
    yield("Dave", "hello")
    yield("Andy", "goodbye")
end
  
who_says_what do |person, phrase|
    puts "#{person} says #{phrase}"
end

def what_does_the_block_say
    puts "Block says: #{yield}"
end

what_does_the_block_say { 'moo' }
what_does_the_block_say { 'muew' }
