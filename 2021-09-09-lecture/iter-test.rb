animals = %w(ant bee cat dog)
pp animals
each_res = animals.each do |animal|
    puts animal
end
for_res = for animal in animals do
    puts animal
end

pp each_res
pp for_res

3.upto(6) { |number| print number }
puts

('a'..'e').each { |character| print character }
