data = [5, 66, 13, 24, 46]
# Все элементы с позицией
data.each_with_index do |number, index|
    puts "#{index}: #{number}"
end
# Все ли элементы удовлетворяют условию: являются нечётными
puts data.all? { |number| number.odd? }
puts data.any? { |number| number.odd? }
# Выбрать часть элементов массива
pp data.select { |number| number > 40 }
odd_data = data.select { |number| number.odd? }

# Создать новый массив на основе текущего: квадраты чисел
squares_odd_data = odd_data.map { |number| number**2 }
pp squares_odd_data

# Посчитать общую характеристику: сумму чисел
pp squares_odd_data.reduce { |memo, number| number + memo }

pp data.select { |number| number.odd? }
    .map { |number| number**2 }
    .reduce {  |memo, number| number + memo } 
